const preloader = document.querySelector('.lds-ripple');
preloader.style.display = 'block';

class Card {
    constructor(post) {
        this.post = post;
        this.element = this.createCardElement();
        this.element.querySelector('.card__delete').addEventListener('click', () => this.deleteCard(this.post.id));
        this.fetchUserData();
    }

    createCardElement = () => {
        const cardElement = document.createElement('div');
        cardElement.id = this.post.id;
        cardElement.classList.add('card');

        cardElement.innerHTML = `
            <div class="card__wrapper">
                <div class="card__header card--margin">
                    <div class="card__header__left">
                        <p class="card__name card--margin"></p>
                        <p class="card__email card--gray card--margin"></p>
                    </div>
                    <div class="card__header__right">
                        <button type="button" class="card__delete">
                            <svg viewBox="0 0 1024 1024" class="icon" version="1.1" xmlns="http://www.w3.org/2000/svg" fill="#e7e9ea">
                                <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                                <g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g>
                                <g id="SVGRepo_iconCarrier">
                                    <path d="M905.92 237.76a32 32 0 0 0-52.48 36.48A416 416 0 1 1 96 512a418.56 418.56 0 0 1 297.28-398.72 32 32 0 1 0-18.24-61.44A480 480 0 1 0 992 512a477.12 477.12 0 0 0-86.08-274.24z" fill="#e7e9ea"></path>
                                    <path d="M630.72 113.28A413.76 413.76 0 0 1 768 185.28a32 32 0 0 0 39.68-50.24 476.8 476.8 0 0 0-160-83.2 32 32 0 0 0-18.24 61.44zM489.28 86.72a36.8 36.8 0 0 0 10.56 6.72 30.08 30.08 0 0 0 24.32 0 37.12 37.12 0 0 0 10.56-6.72A32 32 0 0 0 544 64a33.6 33.6 0 0 0-9.28-22.72A32 32 0 0 0 505.6 32a20.8 20.8 0 0 0-5.76 1.92 23.68 23.68 0 0 0-5.76 2.88l-4.8 3.84a32 32 0 0 0-6.72 10.56A32 32 0 0 0 480 64a32 32 0 0 0 2.56 12.16 37.12 37.12 0 0 0 6.72 10.56zM726.72 297.28a32 32 0 0 0-45.12 0l-169.6 169.6-169.28-169.6A32 32 0 0 0 297.6 342.4l169.28 169.6-169.6 169.28a32 32 0 1 0 45.12 45.12l169.6-169.28 169.28 169.28a32 32 0 0 0 45.12-45.12L557.12 512l169.28-169.28a32 32 0 0 0 0.32-45.44z" fill="#e7e9ea"></path>
                                </g>
                            </svg>
                        </button>
                    </div>                             
                </div>
                <p class="card__company card--gray card--margin"></p>
                <div class="card__main">
                    <h3 class="card__title card--margin"></h3>
                    <p class="card__content card--margin"></p>
                </div>
            </div>
        `;

        return cardElement;
    }

    fetchUserData = () => {
        fetch('https://ajax.test-danit.com/api/json/users')
            .then(res => res.json())
            .then(users => {
                preloader.style.display = 'none';
                this.user = users;
                this.addContentCardData();
            })
            .catch(err => console.error(err));
    }
    addContentCardData = () => {
        const user = this.user.find(el => el.id === this.post.userId);
        if (user) {
            const {name, email, company} = user;
            this.element.querySelector('.card__name').textContent = name;
            this.element.querySelector('.card__email').textContent = email;
            this.element.querySelector('.card__company').textContent = company.name;
        }

        this.element.querySelector('.card__title').textContent = this.post.title;
        this.element.querySelector('.card__content').textContent = this.post.body;
    }
    deleteCard = (postId) => {
        fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
            method: 'DELETE'
        })
            .then(res => {
                if (res.status !== 200) {
                    throw new Error('Запит не вдалося виконати');

                }
                document.getElementById(`${postId}`).remove();
            })
            .catch(error => console.error('Помилка:', error));

    }

}

fetch('https://ajax.test-danit.com/api/json/posts')
    .then(res => res.json())
    .then(posts => {
        const cardContainer = document.querySelector('.card-container');
        preloader.style.display = 'none';
        posts.forEach(post => {
            const card = new Card(post);
            cardContainer.appendChild(card.element);
        });
    })
    .catch(err => console.error(err));

